try {
	erpnext.pos.PointOfSale =  erpnext.pos.PointOfSale.extend({
		make_item_list: function () {
			var me = this;
			if (!this.price_list) {
				frappe.msgprint(__("Price List not found or disabled"));
				return;
			}
	
			me.item_timeout = null;
	
			var $wrap = me.wrapper.find(".item-list");
			me.wrapper.find(".item-list").empty();
	
			if (this.items.length > 0) {
				$.each(this.items, function(index, obj) {
					if(index < me.page_len) {
						$(frappe.render_template("vanilla_pos_item", {
							item_code: obj.name,
							item_price: format_currency(me.price_list_data[obj.name], me.frm.doc.currency),
							item_name: obj.name === obj.item_name ? "" : obj.item_name,
							item_image: obj.image,
							item_variants: obj.attributes && obj.attributes.length ? obj.attributes.join(): '',
							color: frappe.get_palette(obj.item_name),
							abbr: frappe.get_abbr(obj.item_name)
						})).tooltip().appendTo($wrap);
					}
				});
	
				$wrap.append(`
					<div class="image-view-item btn-more text-muted text-center">
						<div class="image-view-body">
							<i class="mega-octicon octicon-package"></i>
							<div>Load more items</div>
						</div>
					</div>
				`);
	
				me.toggle_more_btn();
			} else {
				$("<p class='text-muted small' style='padding-left: 10px'>"
					+__("Not items found")+"</p>").appendTo($wrap)
			}
	
			if (this.items.length == 1
				&& this.serach_item.$input.val()) {
				this.serach_item.$input.val("");
				this.add_to_cart();
			}
		},
	});
} catch(e) {}